<?php

namespace App\Entity;

use App\Repository\ClientTravelRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClientTravelRepository::class)
 */
class ClientTravel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $client_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $travel_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClientId(): ?int
    {
        return $this->client_id;
    }

    public function setClientId(int $client_id): self
    {
        $this->client_id = $client_id;

        return $this;
    }

    public function getTravelId(): ?int
    {
        return $this->travel_id;
    }

    public function setTravelId(int $travel_id): self
    {
        $this->travel_id = $travel_id;

        return $this;
    }
}
