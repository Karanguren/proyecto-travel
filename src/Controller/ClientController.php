<?php

namespace App\Controller;

use App\Repository\ClientRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ClientController
 * @package App\Controller
 *
 * @Route(path="/api/")
 */

class ClientController extends AbstractController
{
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }



    /**
     * @Route("clients", name="add_client", methods={"POST"})
     */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $identification = $data['identification'];
        $name = $data['name'];
        $birth_date = $data['birth_date'];
        $phone_number = $data['phone_number'];

        if (empty($identification) || empty($name) || empty($birth_date) || empty($phone_number)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }

        $client = $this->clientRepository->saveClient($identification, $name, $birth_date, $phone_number);

        return new JsonResponse(['client' => $client->getId()], Response::HTTP_CREATED);
    }

    /**
     * @Route("clients", name="get_all_clients", methods={"GET"})
     */
    public function index(): JsonResponse
    {
        $clients = $this->clientRepository->findAll();
        $data = [];

        foreach ($clients as $client) {
            $data[] = [
                'id' => $client->getId(),
                'identification' => $client->getIdentification(),
                'name' => $client->getName(),
                'birth_date' => $client->getBirthDate()->format('Y-m-d'),
                'phone_number' => $client->getPhoneNumber(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("clients/{id}", name="get_one_client", methods={"GET"})
     */
    public function get(string $id): object
    {
        $client = $this->clientRepository->findOneBy(['id' => $id]);

        $data = [
            'id' => $client->getId(),
            'identification' => $client->getIdentification(),
            'name' => $client->getName(),
            'birth_date' => $client->getBirthDate()->format('Y-m-d'),
            'phone_number' => $client->getPhoneNumber(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("clients/{id}", name="update_clients", methods={"PUT"})
     */
    public function update($id, Request $request): JsonResponse
    {
        $client = $this->clientRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(), true);

        empty($data['identification']) ? true : $client->setIdentification($data['identification']);
        empty($data['name']) ? true : $client->setName($data['name']);
        empty($data['birth_date']) ? true : $client->setBirthDate($data['birth_date']);
        empty($data['phone_number']) ? true : $client->setPhoneNumber($data['phone_number']);

        $updatedClient = $this->clientRepository->updateClient($client);

		return new JsonResponse(['status' => 'client updated!'], Response::HTTP_OK);
    }

    /**
     * @Route("clients/{id}", name="delete_clients", methods={"DELETE"})
     */
    public function delete(string $id): JsonResponse
    {
        $client = $this->clientRepository->findOneBy(['id' => $id]);

        $this->clientRepository->removeClient($client);

        return new JsonResponse(['status' => 'client deleted'], Response::HTTP_OK);
    }
}
