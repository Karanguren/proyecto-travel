<?php

namespace App\Controller;

use App\Repository\ClientTravelRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class TravelController
 * @package App\Controller
 *
 * @Route(path="/api/")
 */

class TravelController extends AbstractController
{
    private $clientTravelRepository;

    public function __construct(ClientTravelRepository $clientTravelRepository)
    {
        $this->clientTravelRepository = $clientTravelRepository;
    }

    /**
     * @Route("travels", name="get_all_travels", methods={"GET"})
     */
    public function index() {
        $response = new JsonResponse();
        $response->setData([
            'data' => [
                [
                    'id' => 1,
                    'code' => '12345',
                    'seat' => '20',
                    'destination' => 'Chile',
                    'source_place' => 'Venezuela',
                    'price' => '700'
                ],
                [
                    'id' => 2,
                    'code' => '54321',
                    'seat' => '10',
                    'destination' => 'Colombia',
                    'source_place' => 'Venezuela',
                    'price' => '300'
                ],
            ]
        ]);
        return $response;
    }

    /**
     * @Route("travel/{id}", name="get_one_travel", methods={"GET"})
     */
    // public function get($id): JsonResponse
    // {
    //     $travel = $this->travelRepository->findOneBy(['id' => $id]);

    //     $data = [
    //         'id' => $travel->getId(),
    //         'code' => $travel->getCode(),
    //         'seat' => $travel->getSeat(),
    //         'destination' => $travel->getDestination(),
    //         'source_place' => $travel->getSourcePlace(),
    //         'price' => $travel->getPhoneNumber(),
    //     ];

    //     return new JsonResponse($data, Response::HTTP_OK);
    // }
}