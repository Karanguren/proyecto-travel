<?php

namespace App\Repository;

use App\Entity\ClientTravel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ClientTravel|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClientTravel|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClientTravel[]    findAll()
 * @method ClientTravel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientTravelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClientTravel::class);
    }

    // /**
    //  * @return ClientTravel[] Returns an array of ClientTravel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClientTravel
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
